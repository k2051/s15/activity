console.log('Hello World');

let num1 = parseInt(prompt('Input first number:'));
let num2 = parseInt(prompt('Input second number'));
let numSum= num1+num2;

function sum() {
	sum=num1+num2;
	return sum;
}

function diff(){
	diff=num1-num2;
	return diff;
}

function prod(){
	prod=num1*num2;
	return prod;
}

function quot(){
	quot=num1/num2;
	return quot;
}

if (numSum<10) {
	sum();
	console.warn('Sum is '+sum);
	
} else if (numSum>=10 && numSum<20){
	diff();
	alert('Difference is ' +diff);
} else if (numSum>=20 && numSum<30){
	prod();
	alert('Product is ' + prod);
} else if (numSum>=30){
	quot();
	alert('Quotient is ' + quot);
}

// Stretch Goal

let name = prompt('Input Name');
let age = parseInt(prompt('Input Age'));

if (name === null || age === null || name === '' || isNaN(age)) {
	console.log('Are you a time traveller?')
} else{
	console.log('Name is '+name+' with an age of '+age);
}

function isLegalAge(age){
	if (age>=18) {
		alert('You are of legal age');
	} else {
		alert('You are not allowed here.');
	}
}

isLegalAge(age);

switch(age){
	case 18:
	alert('You are now allowed to party');
	break;
	case 21:
	alert('You are now part of the adult society');
	break;
	case 65:
	alert('We thank you for your contribution to society');
	break;
	default:
	alert("Are you sure you're not an alien?");
	break;
}

try{
	alerat(isLegalAge());
}

catch(error){
	console.warn (error.message);
}

finally{
	isLegalAge(age);
}